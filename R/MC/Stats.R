# Author : SV
# date 2015/10/12

############################
# Comparaison d'algorithmes

# lit les donnÃ©es du HC best-improvement
table_mc <- read.table("/home/marvin/Documents/2048.cpp/R/MC/result-mc.csv", header = TRUE, sep = ",")
table_random <- read.table("/home/marvin/Documents/2048.cpp/R/result.csv", header = TRUE, sep = ",")

hist(table_mc$fitness, main="Histogramme MC", xlab="Fitness", ylab="Itérations")
hist(table_random$fitness, main="Histogramme Random", xlab="Fitness", ylab="Itérations")


#plot(bi$fitness, type = "l")
moy_mc<- mean(table_mc$fitness)
moy_random<- mean(table_random$fitness)

maxfit_mc <- max(table_mc$fitness)
maxfit_random <- max(table_random$fitness)

#variance
var_mc <- var(table_mc$fitness)
var_random <- var(table_random$fitness)

#ecart type
sd_mc <- sd(table_mc$fitness)
sd_random <- sd(table_random$fitness)

#norm
hx_mc <- dnorm(table_mc$fitness)
hx_random <- dnorm(table_random$fitness)

x_fitness <- seq(0,10000, by = 0.01)
plot (
  x_fitness, 
  dnorm(x_fitness, mean = moy_random, sd = sd_random), 
  type = "l", 
  col="red", 
  main="Distribution des Fitness", 
  xlab="Fitness", 
  ylab="")
lines (x_fitness, dnorm(x_fitness, mean = moy_mc, sd = sd_mc), type = "l", col="blue")


summary(table_mc$fitness)
summary(table_random$fitness)

plot(table_mc$fitness, type="l", main="Evolution de l'algo MC", xlab="Itérations", ylab="Fitness")
plot(table_random$fitness, type="l", main="Evolution de l'algo Random", xlab="Itérations", ylab="Fitness")

#x_fitness <- seq(5,20, by = 0.01)
#plot (x_fitness, dnorm(x_fitness, mean = moy50, sd = sd50), type = "l", col="red", main="HCFI vs ILS", xlab="Fitness", ylab="")
#lines (x_fitness, dnorm(x_fitness, mean = moyhcfi, sd = sdhcfi), type = "l", col="orange")

