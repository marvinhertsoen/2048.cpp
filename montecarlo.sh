#!/bin/bash

cmake ../ build/
cmake --build build/

for i in `seq 1 5000`;
do
date  ## echo the date at start

timeout 5 ./build/2048
exit_status=$?
if [[ $exit_status -eq 124 ]]; then
    echo exiting..
fi

done
