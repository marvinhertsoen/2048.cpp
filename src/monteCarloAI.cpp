// #include "2048.hpp"

#include "monteCarloAI.hpp"
#include "game.hpp"
#include <iostream>
#include <sstream>
#include <time.h>

/* private */


/* public */

/**
 * [MonteCarloAI::run run a MonteCarlo game]
 */
void MonteCarloAI::run(){
  main_game = Game(GameType::MC, this);
  main_game.startGame();
}


/**
 * Is called by the montecarlo main_game, launch 4 random games (1 by possible move)
 *
 * [MonteCarloAI::nextMove description]
 * @return [id of the best move to play]
 */
int MonteCarloAI::nextMove(){
  int result [4];
  for (int i=0; i < 4 ; i++){
      result[i] = playRandomGame(i); // i = 0,1,2,3 (UP, DOWN, RIGHT, LEFT)
  }
  int best_move = getBiggerFitness(result);
  //std::cout << "bestmove : "<< moveIdToString(best_move) << std::endl;
  return best_move;
}

/**
 * [MonteCarloAI::getBiggerFitness Compares the fitness of the 4 test playouts]
 * @param  tab [int array of fitnesses]
 * @return     [return id of the best move]
 */
int MonteCarloAI::getBiggerFitness(int (&tab)[4]){
  int best_fitness = 0;
  int best_move =0;
  for (int i=0; i < 4 ; i++){
      if (tab[i] > best_fitness){
        best_fitness = tab[i];
        best_move = i;
      }
  }
  return best_move;
}

/**
 * [MonteCarloAI::playRandomGame play 4 random games & gets fitness]
 * @return [fitness of the playout]
 */
int MonteCarloAI::playRandomGame(int first_move){
  Game test;
  test = Game(GameType::RAND, first_move, this);
  // Copy gameboard from main_game to random game
  test.gamePlayBoard = this->main_game.gamePlayBoard;

  int playout_result = test.startGame();
  return playout_result;
}

/**
 * [MonteCarloAI::moveIdToString returns the move string from id [0,1,2,3] ]
 * @param  id [description]
 * @return    [std::string move]
 */
std::string MonteCarloAI::moveIdToString(int id){
  //first_move [0,1,2,3] => UP, DOWN, RIGHT, LEFT
  switch(id){
    case 0 :
    return "UP";
      break;
    case 1 :
    return "DOWN";
      break;
    case 2 :
    return "RIGHT";
      break;
    case 3 :
    return "LEFT";
      break;
    default:
      break;
  }
}
