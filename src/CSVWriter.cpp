// #include "2048.hpp"

#include "CSVWriter.hpp"
#include <iostream>
#include <string>
#include <sstream>
#include <time.h>
#include <fstream>

using namespace std;

/* private */

// std::string name = "test";
string path = ".";


/* public */

CSVWriter::CSVWriter(string path) {
    this->path = path;
}

bool CSVWriter::writeLine(string value) {

  std::ofstream myfile;
  myfile.open (this->path, std::ofstream::app);
  // myfile << "This is the first cell in the first column.\n";
  // myfile << "a,b,c,\n";
  // myfile << "c,s,v,\n";
  // myfile << "1,2,3.456\n";
  // myfile << "semi;colon";
  myfile << value << '\n';
  myfile.close();
  return 0;

  //std::cout << "Bonjour !";
}
