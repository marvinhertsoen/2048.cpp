#ifndef RANDOMAI_H
#define RANDOMAI_H

#include "global.hpp"
#include <string>
#include <vector>



class RandomAI {
  private:
    std::string name;
    // std::vector<Score> testList;

  public:
    bool booltest=false;
    void montest();
    int randomNextMove();
    int rndGeneratorWithBounds(int a, int b);
};

#endif
