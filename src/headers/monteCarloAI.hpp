#ifndef MONTECARLO_H
#define MONTECARLO_H

#include "global.hpp"
#include "game.hpp"
#include "gameboard.hpp"
#include <string>
#include <vector>


class MonteCarloAI {
  private:
    std::string name;
    // std::vector<Score> testList;

  public:
    bool booltest=false;
    Game main_game;

    void run();
    int nextMove();
    int getBiggerFitness(int (&tab)[4]);
    int playRandomGame(int first_move);
    std::string moveIdToString(int id);
};

#endif
